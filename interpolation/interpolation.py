from quicksort import quickSort 
from gerador import Gerador
import timeit

def interpolationSearch(arr, lo, hi, x:int):

   

    if (lo <= hi and x >= int(arr[lo]['idade']) and x <= int(arr[hi]['idade'])):
        try:
            pos = lo + ((hi - lo) // (arr[hi]['idade'] - arr[lo]['idade']) * (x - arr[lo]['idade']))
        except ZeroDivisionError:
            pos = lo + ((hi - lo) // 1 - arr[lo]['idade']) * (x - arr[lo]['idade'])
            
        
        if int(arr[pos]['idade']) == x:

            return pos

        if int(arr[pos]['idade']) < x:

            return interpolationSearch(arr, pos + 1, hi, x)

        if int(arr[pos]['idade']) > x:

            return interpolationSearch(arr, lo, pos - 1, x)

    return -1

generator = Gerador()
db = [ ]
for i in range(10000):
        db.append({
                'nome':generator.geraNome(),
                'sobrenome':generator.geraSobrenome(),
                'idade':generator.geraIdade(),
        })
        
quickSort(db, 0,db.__len__()-1)

n = len(db)

print(db)

x = 50

inicio = timeit.default_timer()

index = interpolationSearch(db, 0, n - 1, x)

fim = timeit.default_timer()

print ('_____ Para 10 mil valores _________ ')
 
if index != -1:

    print("Elemento Encontrado:  ", (fim-inicio))

else:

    print("ELemento não encontrado em: " , (fim-inicio))
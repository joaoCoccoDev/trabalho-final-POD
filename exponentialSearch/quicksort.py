def particiona(v, inicio, fim):
    i = (inicio-1) 
    pivo = v[fim] 
    for j in range(inicio, fim):
       
        if v[j]['nome'] <= pivo['nome']: 
            i = i+1
            v[i], v[j] = v[j], v[i]
       
    v[i+1], v[fim] = v[fim], v[i+1]

    return (i+1)

def quickSort(v, inicio, fim):
    if len(v) == 1:
        return v
    if inicio < fim:
        ip = particiona(v, inicio, fim)
        quickSort(v, inicio, ip-1)
        quickSort(v, ip+1, fim)


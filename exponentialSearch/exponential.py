
from quicksort import quickSort 
from gerador import Gerador
import timeit

def binarySearch(arr, since , final , element):
 
    for i in range(since,final):
        
        if arr[i]['nome'] == element:
        
                return element
     
    return None
        
        
    return
def exponentialSearch(arr:list , size:int , element:int):
    
    bound = 0
   
    while (bound < size) and (arr[bound]['nome'] < element):
        if bound == 0:
            bound = 1 
        bound *= 2
    
 
    try:
        if arr[bound]['nome'] == element:
            return element
    except IndexError:
        return None
        
    return binarySearch(arr  , bound//2, bound , element )
        
        
generator = Gerador()
db = [ ]
for i in range(10000):
        db.append({
                'nome':generator.geraNome(),
                'sobrenome':generator.geraSobrenome(),
                'idade':generator.geraIdade(),
        })
        
quickSort(db, 0,db.__len__()-1)

print(db)

nome = input('Entre com um nome: ')

inicio = timeit.default_timer()

val = exponentialSearch(db, len(db) ,  nome ) 
    
fim = timeit.default_timer()
    
   
print ('_____ Para 10 mil valores _________ ')

if val != None:
    print('Encontrado em: ', (fim-inicio))     
else: 
    print('Não Encontrado em: ', (fim-inicio))


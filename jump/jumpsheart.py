import math


def jumpSearch( arr:list , x:any , n:int ):
     
    step = math.sqrt(n)
     
    prev = 0

    while arr[int(min(step, n)-1)]['nome'] < x:
        prev = step
        step += math.sqrt(n)
        if prev >= n:
            return -1
 
    while arr[int(prev)]['nome'] < x:
        prev += 1
         
        if prev == min(step, n):
            return -1
     
    if arr[int(prev)]['nome'] == x:
        return prev
     
    return -1
 


from gerador import Gerador
from quicksort import quickSort
from jumpsheart import jumpSearch
import timeit

db  = [ ]
generator = Gerador()
for i in range(10000):
        db.append({
                'nome':generator.geraNome(),
                'sobrenome':generator.geraSobrenome(),
                'idade':generator.geraIdade(),
        })
quickSort(db, 0,db.__len__()-1)

name_search = input('Digite uma nome a serbuscado: ')

inicio = timeit.default_timer()

val = jumpSearch(db,name_search,len(db)) 

fim = timeit.default_timer()

print ('_____ Para 10 mil valores _________ ')

if int(val) != int(-1):
    print('Encontrado em: ', (fim-inicio))     
else: 
    print('Não Encontrado em: ', (fim-inicio))



from  gerador import Gerador
import timeit

class BSTNode:
    def __init__(self, val=None):
        self.left = None
        self.right = None
        self.val = val

    def insert(self, val):
        if not self.val:
            self.val = val
            return

        if self.val['nome'] == val['nome']:
            return

        if val['nome'] < self.val['nome']:
            if self.left:
                self.left.insert(val)
                return
            self.left = BSTNode(val)
            return

        if self.right:
            self.right.insert(val)
            return
        self.right = BSTNode(val)


    def exists(self, val):
        if val == self.val['nome']:
            return True

        if val < self.val['nome']:
            if self.left == None:
                return False
            return self.left.exists(val)

        if self.right == None:
            return False
        return self.right.exists(val)

 
 
    def mostraarvore(self, no_atual):
        if no_atual != None:
            self.mostraarvore( no_atual.left)
            print(f'{no_atual.val}', end=' ')
            self.mostraarvore(no_atual.right)


t = BSTNode()

generator = Gerador()
for i in range(10000):
	    t.insert({
                'nome':generator.geraNome(),
                'sobrenome':generator.geraSobrenome(),
                'idade':generator.geraIdade(),
        })
     
t.mostraarvore(t)

nome = input('Entre com um nome')

inicio = timeit.default_timer()

val = t.exists(nome)
fim = timeit.default_timer()

print(
'---Busca em arvore binaria com 10 mil registros ------ '

)

if val:
    print('Encontrado em: ', (fim-inicio))     
else: 
    print('Não Encontrado em: ', (fim-inicio))
    